/**
 * Created by marcel on 5/16/14.
 */
public class SSimpleLoop01 {
    public static void l() {
        MissileBattery r1 = new MissileBattery(97);
        int k = 5;
        int i = 94;
        while(k < 444234212) {
            k++;
        }
        while(i < 96) {
            i++;
        }
        r1.fire(i);
        r1.fire(k);
    }
}
