import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.PrintStream;
import java.util.Collection;
import java.util.LinkedList;

import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;


import ch.ethz.sae.Verifier;

@RunWith(Parameterized.class)
public class Test {
	
	private String name;
	// constructor for the instance - name is the parameterized argument
	public Test(String name) {
		this.name = name;
	}
	
	// list of all parameters. For each an instance of Test 
	// below will be created, with name assigned to the parameter
	@Parameters(name = "{index}: File {0}")
	public static Collection<String[]> fileArgs() {
		File testFilesDir = new File("src");
		File[] list = testFilesDir.listFiles();
		LinkedList<String[]> fileNames = new LinkedList<String[]>();
		System.out.println("TestFiles found:");
		for (File f: list) {
			String name = f.getName();
			if (f.isFile() && !name.equals("MissileBattery.java") && !name.equals("Test.java")) {
				fileNames.add(new String[]{name.substring(0, name.length()-5)});
				System.out.println(name.substring(0, name.length()-5));
			}
		}
		System.out.println("\n*Refresh the project after adding new java test files if not done in Eclipse.");
		System.out.println("*SLoop.java can result in a runtime error despite being a valid test file. This should be a bug/limitation of the test suite only.\n");
		System.out.println("Uncomment in the test method almost at the end of Test.java the annotated line to\nsee the whole soot output for each test case.");
		return fileNames;
	}
	
	@org.junit.Test
	public void test() {
		// Create a stream to hold the output
	    ByteArrayOutputStream baos = new ByteArrayOutputStream();
	    PrintStream ps = new PrintStream(baos);
	    // IMPORTANT: Save the old System.out!
	    PrintStream old = System.out;
	    // Tell Java to use your special stream
	    System.setOut(ps);
	    
	    String[] file = new String[] {name};
	    // Execute the verifier
	    // Print some output: goes to your special stream
		Verifier.main(file);
	    
	    // Put things back
	    System.out.flush();
	    System.setOut(old);
	    
	    
	    /* */
	    /* Remove comment of next line for full output*/
	    // System.out.println(baos.toString());
	    /* */
	    
	    
	    // Split the returned message per Line
	    String[] outputPerLine = baos.toString().split("\\r?\\n");
	    //Check if safe or not and assert appropriately
	    if (name.charAt(0) == 'S') {
	    	assertEquals("Program " + file[0] + " is SAFE", outputPerLine[outputPerLine.length-1]);
	    }
	    else if (name.charAt(0) == 'U') {
	    	assertEquals("Program " + file[0] + " is UNSAFE", outputPerLine[outputPerLine.length-1]);
	    }
	    else fail("Please make sure that the file starts with U for Unsafe or S for Safe.");
	}

}
