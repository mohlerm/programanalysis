/**
 * Created by marcel on 5/19/14.
 */
public class UNestedWhile02 {
    public static void l() {
        MissileBattery r = new MissileBattery(6);
        int i = 2;
        int k = 0;
        while (k < 3) {
            k++;
            while (i < 2) {
                i++;
            }
            i=7;

        }
        r.fire(i);
    }
}
