public class UOwnWhileIf {
    public static void t21_safe() {
        MissileBattery a = new MissileBattery(4);
        MissileBattery b = a;
        int i = 1;
        int j = 1;
        while(i < 10000) {
            i++;
        }
        int k = 1;
        while(j < 3) {
            if(i > 11000) {
                k = 2;
            } else {
                k = 6;
            }
            j++;
        }
        b.fire(k);
    }
}
