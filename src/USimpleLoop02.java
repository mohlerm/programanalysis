/**
 * Created by marcel on 5/16/14.
 */
public class USimpleLoop02 {
    public static void l() {
        MissileBattery r1 = new MissileBattery(97);
        int i = 80;


        while(i<100) {
            i++;
        }

        r1.fire(i);
    }
}
