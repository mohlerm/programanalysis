package ch.ethz.sae;

import java.util.*;

import apron.*;
import ch.ethz.sae.AWrapper;
import ch.ethz.sae.Tuple;
import soot.IntegerType;
import soot.Local;
import soot.RefType;
import soot.SootClass;
import soot.Unit;
import soot.Value;
import soot.jimple.BinopExpr;
import soot.jimple.ConditionExpr;
import soot.jimple.DefinitionStmt;
import soot.jimple.IntConstant;
import soot.jimple.Stmt;
import soot.jimple.internal.*;
import soot.toolkits.graph.UnitGraph;
import soot.toolkits.scalar.ForwardBranchedFlowAnalysis;
import soot.util.Chain;

// Implement your numerical analysis here.
public class Analysis extends ForwardBranchedFlowAnalysis<AWrapper> {

    private static boolean DEBUG = false;
    private static int WIDENINGCONST = 1000;
    private static int WIDENINGPLUS = 1;
    private void recordIntLocalVars() {
        if(DEBUG) System.out.println(new Object(){}.getClass().getEnclosingMethod().getName());

        Chain<Local> locals = g.getBody().getLocals();

        int count = 0;
        Iterator<Local> it = locals.iterator();
        while (it.hasNext()) {
            JimpleLocal next = (JimpleLocal) it.next();
            // String name = next.getName();
            if (next.getType() instanceof IntegerType)
                count += 1;
        }

        local_ints = new String[count];

        int i = 0;
        it = locals.iterator();
        while (it.hasNext()) {
            JimpleLocal next = (JimpleLocal) it.next();
            String name = next.getName();
            if (next.getType() instanceof IntegerType)
                local_ints[i++] = name;
        }
    }

    /* Build an environment with integer variables. */
    public void buildEnvironment() {
        if(DEBUG) System.out.println(new Object(){}.getClass().getEnclosingMethod().getName());

        recordIntLocalVars();

        String ints[] = new String[local_ints.length];

		/* add local ints */
        for (int i = 0; i < local_ints.length; i++) {
            ints[i] = local_ints[i];
        }

        env = new Environment(ints, reals);
    }

    /* Instantiate a domain. */
    private void instantiateDomain() {
        if(DEBUG) System.out.println(new Object(){}.getClass().getEnclosingMethod().getName());
        // Initialize variable 'man' to Polyhedra
        man = new Polka(true);

    }

    /* === Constructor === */
    public Analysis(UnitGraph g, SootClass jc) {
        super(g);

        this.g = g;
        this.jclass = jc;

        buildEnvironment();
        instantiateDomain();

        // initialize countmap
        wideningCountMap = new HashMap<Stmt, Tuple<Integer, Integer>>();

        fixPoint = new HashMap<JVirtualInvokeExpr, Abstract1>();
    }

    void run() {
        doAnalysis();
    }

    // call this if you encounter statement/expression which you should not be handling
    static void unhandled(String what) {
        if(DEBUG) System.out.println(new Object(){}.getClass().getEnclosingMethod().getName());
        System.err.println("Can't handle " + what);
        System.exit(1);
    }

    // handle conditionals
    private void handleIf(AbstractBinopExpr expr, Abstract1 in, AWrapper ow,
                          AWrapper ow_branchout) throws ApronException {
        if(DEBUG) System.out.println(new Object(){}.getClass().getEnclosingMethod().getName());

        if(in == null) {
            ow.set(null);
            ow_branchout.set(null);
            return;
        }
        

        Texpr1Node lAr = null;
        Texpr1Node rAr = null;
        Texpr1Node resultNodeLR, resultNodeRL;

        Tcons1 constraint = null;
        Tcons1 constraintBranchout = null;
        Tcons1 constraint1=null,constraintBranchout1=null;

        Tcons1[] fallout = new Tcons1[1];
        Tcons1[] branchout = new Tcons1[1];

        Value left = expr.getOp1();
        Value right = expr.getOp2();
        
        if((left instanceof JimpleLocal && left.getType() instanceof RefType) || (right instanceof JimpleLocal && right.getType() instanceof RefType)){
        	ow.set(in);
        	ow_branchout.set(in);
        	return;
        }
     

        // check waether left/right are constants or local variables
        if(left instanceof IntConstant) {
            lAr = new Texpr1CstNode(new MpqScalar(((IntConstant) left).value));
        }
        else if(left instanceof JimpleLocal) {
            lAr = new Texpr1VarNode(((JimpleLocal) left).getName());
        }
        if(right instanceof IntConstant) {
            rAr = new Texpr1CstNode(new MpqScalar(((IntConstant) right).value));
        }
        else if(right instanceof JimpleLocal) {
            rAr = new Texpr1VarNode(((JimpleLocal) right).getName());
        }

        resultNodeLR = new Texpr1BinNode(Texpr1BinNode.OP_SUB, lAr, rAr);
        resultNodeRL = new Texpr1BinNode(Texpr1BinNode.OP_SUB, rAr, lAr);
        if(expr instanceof JEqExpr) {
            constraint = new Tcons1(env, Tcons1.SUP, resultNodeRL);
            constraintBranchout = new Tcons1(env, Tcons1.SUPEQ, resultNodeLR);
            constraint1 = new Tcons1(env, Tcons1.SUP, resultNodeLR);
            constraintBranchout1 = new Tcons1(env, Tcons1.SUPEQ, resultNodeRL);
        }
        else if(expr instanceof JNeExpr) {
            constraint = new Tcons1(env, Tcons1.SUP, resultNodeRL);
            constraintBranchout = new Tcons1(env, Tcons1.SUPEQ, resultNodeLR);
            constraint1 = new Tcons1(env, Tcons1.SUP, resultNodeLR);
            constraintBranchout1 = new Tcons1(env, Tcons1.SUPEQ, resultNodeRL);
        }
        else if(expr instanceof JGeExpr) {
            constraint = new Tcons1(env, Tcons1.SUP, resultNodeRL);
            constraintBranchout = new Tcons1(env, Tcons1.SUPEQ, resultNodeLR);
        }
        else if(expr instanceof JLeExpr) {
            constraint = new Tcons1(env, Tcons1.SUP, resultNodeLR);
            constraintBranchout = new Tcons1(env, Tcons1.SUPEQ, resultNodeRL);
        }
        else if(expr instanceof JGtExpr) {
            constraint = new Tcons1(env, Tcons1.SUPEQ, resultNodeRL);
            constraintBranchout = new Tcons1(env, Tcons1.SUP, resultNodeLR);
        }
        else if(expr instanceof JLtExpr) {
            constraint = new Tcons1(env, Tcons1.SUPEQ, resultNodeLR);
            constraintBranchout = new Tcons1(env, Tcons1.SUP, resultNodeRL);
        }


        Abstract1 a1,a2,a3,a4,r1,r2;
        fallout[0] = constraint;
        branchout[0] = constraintBranchout;
        a1 = new Abstract1(man, fallout);
        a2 = new Abstract1(man, branchout);
        
        
        /*
         * UGLY CODE
         */
//        if(expr instanceof JNeExpr) {
//            fallout[0] = constraint1;
//            branchout[0] = constraintBranchout1;
//            a3 = new Abstract1(man, fallout);
//            a4 = new Abstract1(man, branchout);
//            r1 = ow.get().meetCopy(man, a2);
//            ow.get().meet(man, a4);
//            ow.get().join(man, r1);
//
//            r1 = ow_branchout.get().meetCopy(man, a1);
//            ow_branchout.get().meet(man, a3);
//            ow_branchout.get().join(man, r1);
//        }
        if (expr instanceof JEqExpr || expr instanceof JNeExpr){
            fallout[0] = constraint1;
            branchout[0] = constraintBranchout1;
            a3 = new Abstract1(man, fallout);
            a4 = new Abstract1(man, branchout);
            r1 = ow_branchout.get().meetCopy(man, a2);
            ow_branchout.get().meet(man, a4);
            ow_branchout.get().meet(man, r1);

            r1 = ow.get().meetCopy(man, a1);

            ow.get().meet(man, a3);
            ow.get().join(man, r1);

            // swap ow and ow branchout if NE
            if(expr instanceof JNeExpr) {
                Abstract1 temp;
                temp = ow.get();
                ow.set(ow_branchout.get());
                ow_branchout.set(temp);
            }

        }
        else {
            ow.get().meet(man, a1);
            ow_branchout.get().meet(man, a2);
        }


        Stmt lastIfStmt = lastStmt.get(lastStmt.size() - 1);
        if (!whileCounter.containsKey(lastIfStmt)) {
            whileCounter.put(lastIfStmt, 0);
        }
        if(whileCounter.get(lastIfStmt) >= 0) {
            if(ow_branchout.get().isBottom(man)) {
                whileCounter.put(lastIfStmt, whileCounter.get(lastIfStmt) + 1);
            } else {
                whileCounter.put(lastIfStmt, -1);
            }
        }

        if(ow.get().isBottom(man) == true) {
            ow.set(null);
        }
        if(ow_branchout.get().isBottom(man) == true) {
            ow_branchout.set(null);
        }

        if(whileCounter.get(lastIfStmt) > WIDENINGCONST+WIDENINGPLUS) {
            ow_branchout.set(null);
        }

    }

    // handle assignments
    private void handleDef(Abstract1 o, Value left, Value right)
            throws ApronException {
        if(DEBUG) System.out.println(new Object(){}.getClass().getEnclosingMethod().getName());

        if(o == null) {
            return;
        }

        Texpr1Node lAr = null;
        Texpr1Node rAr = null;
        Texpr1Intern xp = null;
        Texpr1BinNode binNode = null;

        if((left instanceof JimpleLocal && left.getType() instanceof RefType) || (right instanceof JimpleLocal && right.getType() instanceof RefType)){
            return;
        }

        if (left instanceof JimpleLocal) {
            String varName = ((JimpleLocal) left).getName();

            if (right instanceof IntConstant) {
				/* Example of handling assignment to an integer constant */
                IntConstant c = ((IntConstant) right);

                rAr = new Texpr1CstNode(new MpqScalar(c.value));
                xp = new Texpr1Intern(env, rAr);

                o.assign(man, varName, xp, null);

            } else if (right instanceof JimpleLocal) {
				/* Example of handling assignment to a local variable */
                if (env.hasVar(((JimpleLocal) right).getName())) {
                    rAr = new Texpr1VarNode(((JimpleLocal) right).getName());
                    xp = new Texpr1Intern(env, rAr);
                    o.assign(man, varName, xp, null);
                }
            } else if (right instanceof BinopExpr) {
                Value op1 = ((BinopExpr) right).getOp1();
                Value op2 = ((BinopExpr) right).getOp2();


                if(op1 instanceof JimpleLocal) {
                    lAr = new Texpr1VarNode(((JimpleLocal) op1).getName());
                }
                else if(op1 instanceof IntConstant) {
                    lAr = new Texpr1CstNode(new MpqScalar(((IntConstant) op1).value));
                }

                if(op2 instanceof JimpleLocal) {
                    rAr = new Texpr1VarNode(((JimpleLocal) op2).getName());
                }
                else if(op2 instanceof IntConstant) {
                    rAr = new Texpr1CstNode(new MpqScalar(((IntConstant) op2).value));
                }


                if(right instanceof JMulExpr) {
                    binNode = new Texpr1BinNode(Texpr1BinNode.OP_MUL, lAr, rAr);
                }
                else if(right instanceof JSubExpr) {
                    binNode = new Texpr1BinNode(Texpr1BinNode.OP_SUB, lAr, rAr);
                }
                else if(right instanceof JAddExpr) {
                    binNode = new Texpr1BinNode(Texpr1BinNode.OP_ADD, lAr, rAr);
                }
                else if(right instanceof JDivExpr) {
                    binNode = new Texpr1BinNode(Texpr1BinNode.OP_DIV, lAr, rAr);
                }
                else {
                    unhandled("Invalid Binary Expression: " + right);
                }

                xp = new Texpr1Intern(env, binNode);
                o.assign(man, varName, xp, null);

            } else {
                o = newInitialFlow().get();
            }

        }
    }

    @Override
    protected void flowThrough(AWrapper current, Unit op,
                               List<AWrapper> fallOut, List<AWrapper> branchOuts) {
        if(DEBUG) System.out.println(new Object(){}.getClass().getEnclosingMethod().getName());

        Stmt s = (Stmt) op;
        Abstract1 in = ((AWrapper) current).get();
        lastStmt.add(s);
        Abstract1 o;



        try {
            if((s instanceof JInvokeStmt) && current.get().isBottom(man) && op.toString().contains("<MissileBattery: void fire")) {
                unreachedFireCalls.add((JVirtualInvokeExpr) s.getInvokeExpr());
            }
            if((s instanceof JInvokeStmt) && !current.get().isBottom(man) && op.toString().contains("<MissileBattery: void fire") ) {
                if(unreachedFireCalls.contains(s.getInvokeExpr())){
                    unreachedFireCalls.remove(s.getInvokeExpr());
                }
            }
            o = new Abstract1(man, in);

            Abstract1 o_branchout = new Abstract1(man, in);

            AWrapper ow = new AWrapper(o, man);
            AWrapper ow_branchout = new AWrapper(o_branchout, man);

            if (s instanceof DefinitionStmt) {
                handleDef(o, ((DefinitionStmt) s).getLeftOp(), ((DefinitionStmt) s).getRightOp());

            } else if (s instanceof JIfStmt) {

                handleIf((AbstractBinopExpr) ((JIfStmt) s).getCondition(), in, ow, ow_branchout);
            }


            if(s instanceof DefinitionStmt && Arrays.asList(local_ints).contains(((JimpleLocal)((DefinitionStmt) s).getLeftOp()).getName())) {
                if (wideningCountMap.containsKey(s)) {
                    JimpleLocal leftOp = (JimpleLocal) ((DefinitionStmt) s).getLeftOp();
                    Interval newInterval = ow.get().getBound(man, leftOp.getName());
                    Interval oldInterval = in.getBound(man, leftOp.getName());
                    int newLowerBound = getInfForInterval(newInterval);
                    int newUpperBound = getSupForInterval(newInterval);
                    int oldLowerBound = getInfForInterval(oldInterval);
                    int oldUpperBound = getSupForInterval(oldInterval);

                    Texpr1CstNode lowerCstNode;
                    Texpr1Intern lowerIntern;
                    Texpr1CstNode upperCstNode;
                    Texpr1Intern upperIntern;

                    if(newLowerBound < oldLowerBound) {
                        wideningCountMap.get(s).x += 1;
                        MpqScalar ms;
                        if(wideningCountMap.get(s).x >= WIDENINGCONST) {
                            ms = new MpqScalar();
                            ms.setInfty(-1);
                            //ow.set(in.widening(man, ow.get()));
                            //if(DEBUG) System.out.println(">>>>>>>>>> WIDENING: " + this.getPossibleValues(leftOp, s));
                        } else {
                            ms = new MpqScalar(newLowerBound);
                        }
                        lowerCstNode = new Texpr1CstNode(ms);
                        lowerIntern = new Texpr1Intern(env, lowerCstNode);
                    }
                    if(newUpperBound > oldUpperBound) {
                        wideningCountMap.get(s).y += 1;
                        MpqScalar ms;
                        if(wideningCountMap.get(s).y >= WIDENINGCONST) {
                            ms = new MpqScalar();
                            ms.setInfty(-1);
                            //ow.set(in.widening(man, ow.get()));
                            //if(DEBUG) System.out.println(">>>>>>>>>> WIDENING: " + this.getPossibleValues(leftOp, s));
                        } else {
                            ms = new MpqScalar(newLowerBound);
                        }
                        upperCstNode = new Texpr1CstNode(ms);
                        upperIntern = new Texpr1Intern(env, upperCstNode);
                    }

                    // o.assign(man, varName, xp, null);


                } else {
                    wideningCountMap.put(s, new Tuple<Integer, Integer>(0, 0));
                }


            }


            if(ow.get() != null) {
                for (AWrapper f : fallOut) {
                    f.copy(new AWrapper(ow.get(), man));
                }
            }

            if(ow_branchout.get() != null) {
                for (AWrapper f : branchOuts) {
                    f.copy(new AWrapper(ow_branchout.get(), man));
                }
            }

            if(s instanceof JInvokeStmt) {
                if(s.getInvokeExpr() instanceof  JVirtualInvokeExpr) {
                    fixPoint.put((JVirtualInvokeExpr) s.getInvokeExpr(), ow.get().joinCopy(man, ow_branchout.get()));
                }
            }
            if(DEBUG) System.out.println(fixPoint.toString());


        } catch (ApronException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    // Initialize starting label (top)
    @Override
    protected AWrapper entryInitialFlow() {
        if(DEBUG) System.out.println(new Object(){}.getClass().getEnclosingMethod().getName());
        try {
            return new AWrapper(new Abstract1(man, env), man);
        } catch (ApronException e) {
            e.printStackTrace();
        }
        return null;
    }

    // Implement Join
    @Override
    protected void merge(AWrapper src1, AWrapper src2, AWrapper trg) {
        if(DEBUG) System.out.println(new Object(){}.getClass().getEnclosingMethod().getName());
        try {
            boolean flag = false;
            for(Stmt l : lastStmt) {
                if(wideningCountMap.get(l) != null && (wideningCountMap.get(l).x >0 || wideningCountMap.get(l).y >0)) {
                    flag = true;
                }
            }
            lastStmt.clear();
        	//if(!src2.get().isBottom(man) && wideningCountMap.get(lastStmt) != null && (wideningCountMap.get(lastStmt).x > 0 || wideningCountMap.get(lastStmt).y > 0)) {
        	//if(!src2.get().isBottom(man)) {
//            if(!src2.get().isBottom(man) && flag) {
//        			trg.set(src2.get());
//        		return;
//        	}
        		
            if(src1.get() == null && src2.get() == null) {
                trg.set(null);
            } else if(src1.get() == null) {
                trg.set(src2.get());
            } else if(src2.get() == null) {
                trg.set(src1.get());
            } else {
                trg.set(src1.get().joinCopy(man, src2.get()));
            }
        } catch (ApronException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected  void merge(Unit op, AWrapper src1, AWrapper src2, AWrapper trg) {
        if(!loopCounter.containsKey(op)) {
            loopCounter.put(op, 0);
        }
        loopCounter.put(op, loopCounter.get(op)+1);

        if(loopCounter.get(op) > WIDENINGCONST) {
            try {
                trg.set(src1.get().widening(man, src2.get()));
            } catch (ApronException e) {
                e.printStackTrace();
            }
        } else {
            merge(src1,src2,trg);
        }

//        if(loopMap.containsKey(op))
//        {
//            LoopCount loopCount = loopMap.get(op);
//            if(loopCount.getCount() > WideningCount)
//            {
//                applyIntervalWidening(src1,trg,trg);
//                int i = 0;
//            }
//        }
    }

    // Initialize all labels (bottom)
    @Override
    protected AWrapper newInitialFlow() {
        if(DEBUG) System.out.println(new Object(){}.getClass().getEnclosingMethod().getName());
        try {
            return new AWrapper(new Abstract1(man, env, true), man);
        } catch (ApronException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void copy(AWrapper source, AWrapper dest) {
        if(DEBUG) System.out.println(new Object(){}.getClass().getEnclosingMethod().getName());
        dest.copy(source);
    }

	/* It may be useful for widening */
	/*
	 * private HashSet<Stmt> backJumps = new HashSet<Stmt>(); private
	 * HashSet<Integer> backJumpIntervals = new HashSet<Integer>();
	 */

    public List<Integer> getPossibleValues(Value in, JVirtualInvokeExpr expr) {
        List<Integer> possibleValues = new ArrayList<Integer>();
        try {
            if(in instanceof JimpleLocal) {
                Interval ival = fixPoint.get(expr).getBound(man, in.toString());
                int lowerBound = getInfForInterval(ival);
                int upperBound = getSupForInterval(ival);
                for (int i = lowerBound; i <= upperBound; i++) {
                    Scalar leftLowerBound = new MpqScalar();
                    leftLowerBound.setInfty(-1);
                    Scalar leftUpperBound = new MpqScalar(i-1);
                    Scalar rightLowerBound = new MpqScalar(i+1);
                    Scalar rightUpperBound = new MpqScalar();
                    rightUpperBound.setInfty(1);
                    Boolean inLeftInterval = fixPoint.get(expr).satisfy(man, ((JimpleLocal) in).getName(), new Interval(leftLowerBound, leftUpperBound));
                    Boolean inRightInterval = fixPoint.get(expr).satisfy(man, ((JimpleLocal) in).getName(), new Interval(rightLowerBound, rightUpperBound));
                    if(!(inLeftInterval || inRightInterval)) {
                        possibleValues.add(i);
                    }
                }
            } else if (in instanceof IntConstant) {
                possibleValues.add(((IntConstant) in).value);
            }
        } catch (ApronException e ) {
            e.printStackTrace();
        }
        if (DEBUG) System.out.println(possibleValues);
        return possibleValues;
    }

    protected int getInfForInterval(Interval ival) {
        int result;
        try {
            result  = Integer.parseInt(ival.inf().toString());
        } catch (Exception e) {
            result =-1;
        }
        return result;
    }
    protected int getSupForInterval(Interval ival) {
        int result;
        try {
            result  = Integer.parseInt(ival.sup().toString());
        } catch (Exception e) {
            result = 100;
        }
        return result;
    }

    public static Manager man;
    private Environment env;
    public UnitGraph g;
    public String local_ints[]; // integer local variables of the method
    public static String reals[] = { "foo" };
    public SootClass jclass;
    public HashMap<JVirtualInvokeExpr, Abstract1> fixPoint;
    private HashMap<Stmt, Tuple<Integer, Integer>> wideningCountMap;
    public Set<JVirtualInvokeExpr> unreachedFireCalls = new HashSet<JVirtualInvokeExpr>();
    private List<Stmt> lastStmt = new ArrayList<Stmt>();
    private HashMap<Stmt, Integer> whileCounter = new HashMap<Stmt, Integer>();
    private HashMap<Unit, Integer> loopCounter = new HashMap<Unit, Integer>();
}
