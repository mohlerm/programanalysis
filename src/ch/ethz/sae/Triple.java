package ch.ethz.sae;

/**
 * Created by marcel on 5/12/16.
 */
public class Triple<X, Y, Z> {
    public X x;
    public Y y;
    public Z z;
    public Triple(X x, Y y, Z z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }
}