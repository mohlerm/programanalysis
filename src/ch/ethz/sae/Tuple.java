package ch.ethz.sae;

/**
 * Created by marcel on 5/12/14.
 */
public class Tuple<X, Y> {
    public X x;
    public Y y;
    public Tuple(X x, Y y) {
        this.x = x;
        this.y = y;
    }
}