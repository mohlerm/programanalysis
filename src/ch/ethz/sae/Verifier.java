package ch.ethz.sae;

import java.util.*;

import soot.*;
import soot.jimple.InvokeExpr;
import soot.jimple.internal.JSpecialInvokeExpr;
import soot.jimple.internal.JVirtualInvokeExpr;
import soot.jimple.internal.JimpleLocal;
import soot.jimple.spark.SparkTransformer;
import soot.jimple.spark.pag.PAG;
import soot.toolkits.graph.BriefUnitGraph;

public class Verifier {

	public static void main(String[] args) {
		
		if (args.length != 1) {
			System.err.println("Incorrect usage");
			System.exit(-1);
		}
		
		String analyzedClass = args[0];
		SootClass c = loadClass(analyzedClass);
		PAG pointsToAnalysis = doPointsToAnalysis(c);

		int programCorrectFlag = 1;

		for (SootMethod method : c.getMethods()) {

            if(!method.getName().equals("<init>")) {
                Analysis analysis = new Analysis(new BriefUnitGraph(method.retrieveActiveBody()), c);

                analysis.run();

                //TODO: use analysis results to check safety
                Map<InvokeExpr, SootMethod> m  = pointsToAnalysis.callToMethod;

                // firemap contains all MissileBatteries with an array that counts the fires of each tube
                HashMap<JimpleLocal, int[]> fireMap = new HashMap<JimpleLocal, int[]>();
                // fireCalls contains all calls of fire with the argument and the points-to-set of the caller
                List<Triple<PointsToSet, Value, JVirtualInvokeExpr>> fireCalls = new ArrayList<Triple<PointsToSet, Value, JVirtualInvokeExpr>>();

                for(InvokeExpr e : m.keySet()) {
                    //System.out.println(e.toString());
                    //if(e instanceof JSpecialInvokeExpr && m.get(e).isConstructor() && ((JSpecialInvokeExpr) e).getBaseBox().getValue().getType().toString().equals("MissileBattery"))
                    if(e.toString().contains("<MissileBattery: void <init>(int)>")) {
                    // now we know we're in a constructor definition (hopefully)
                        JimpleLocal key = (JimpleLocal)((JSpecialInvokeExpr) e).getBaseBox().getValue();
                        int size = Integer.parseInt(e.getArgBox(0).getValue().toString());
                        int[] value = new int[size];
                        fireMap.put(key, value);
                    } else if (e.toString().contains("<MissileBattery: void fire(int)>")) {
                        JimpleLocal key = (JimpleLocal)((JVirtualInvokeExpr) e).getBaseBox().getValue();
                        PointsToSet set = pointsToAnalysis.reachingObjects(key);
                        Value arg = (Value) e.getArg(0);
                        fireCalls.add(new Triple<PointsToSet, Value, JVirtualInvokeExpr>(set, arg, (JVirtualInvokeExpr)e));
                    }

                }

                for(Triple<PointsToSet, Value, JVirtualInvokeExpr> call : fireCalls) {
                    for(JimpleLocal battery : fireMap.keySet()) {
                        if(call.x.hasNonEmptyIntersection(pointsToAnalysis.reachingObjects(battery))) {
                            // set fireMap entries with the JimpleLocal argument from call
                            // look up call.y in result from numerical polka analysis

                            if(analysis.unreachedFireCalls.contains(call.z) ) {
                                break;
                            }
                            List<Integer> posVal = analysis.getPossibleValues(call.y, call.z);
                            if(posVal==null) break;
                            int[] firedIndeces = fireMap.get(battery);
                            for(Integer i : posVal) {
                                // check for negative values
                                if (i >= 0 && i < firedIndeces.length) {
                                    firedIndeces[i]++;
                                } else {
                                    programCorrectFlag = 0;
                                }
                            }

                            // check both properties:
                            // a) fire within range
                            // b) not fired twice
                        }

                    }
                }
                for(JimpleLocal battery : fireMap.keySet()) {
                    int[] firedIndeces = fireMap.get(battery);
                    for(int i : firedIndeces) {
                        if (i > 1) {
                            programCorrectFlag = 0;
                        }
                    }
                }

                System.out.println("finish");

                if (programCorrectFlag == 1) {
                    System.out.println("Program " + analyzedClass + " is SAFE");
                } else {
                    System.out.println("Program " + analyzedClass + " is UNSAFE");
                }
            }


        }
	}

	private static SootClass loadClass(String name) {
		SootClass c = Scene.v().loadClassAndSupport(name);
		c.setApplicationClass();
		return c;
	}

	private static PAG doPointsToAnalysis(SootClass c) {
		Scene.v().setEntryPoints(c.getMethods());

		HashMap<String, String> options = new HashMap<String, String>();
		options.put("enabled", "true");
		options.put("verbose", "false");
		options.put("propagator", "worklist");
		options.put("simple-edges-bidirectional", "false");
		options.put("on-fly-cg", "true");
		options.put("set-impl", "double");
		options.put("double-set-old", "hybrid");
		options.put("double-set-new", "hybrid");

		SparkTransformer.v().transform("", options);
		PAG pag = (PAG) Scene.v().getPointsToAnalysis();

		return pag;
	}
}
